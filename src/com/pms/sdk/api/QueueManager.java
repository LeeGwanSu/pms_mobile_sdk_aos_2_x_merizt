package com.pms.sdk.api;

import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.StringRequest;
import com.pms.sdk.push.mqtt.SelfSignedSocketFactory;

public class QueueManager {
    private final int THREAD_POOL_COUNT = 6;

    private static class QueueManagerHolder {
        private static final QueueManager INSTANCE = new QueueManager();
    }

    public static QueueManager getInstance() {
        return QueueManagerHolder.INSTANCE;
    }

    private RequestQueue requestQueue = null;

    private QueueManager() {
        if (requestQueue == null) {
            Network network = new BasicNetwork(new HurlStack(null, SelfSignedSocketFactory.ApiSSLSocketFactory("TLS")));
            this.requestQueue = new RequestQueue(new NoCache(), network, THREAD_POOL_COUNT);
            this.requestQueue.start();
        }

    }

    public void addRequestQueue(StringRequest stringRequest) {
        if (requestQueue != null) {
            this.requestQueue.add(stringRequest);
        }
    }

    public RequestQueue getRequestQueue () {
        return requestQueue;
    }

    @Deprecated
    public void stop() {
        if (requestQueue != null) {
            this.requestQueue.stop();
            requestQueue = null;
        }
    }
}
