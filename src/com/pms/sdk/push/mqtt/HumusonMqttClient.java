package com.pms.sdk.push.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.internal.wire.MqttPingReq;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;

public class HumusonMqttClient extends MqttClient {

	public HumusonMqttClient(String serverURI, String clientId, MqttDefaultFilePersistence mdfp) throws MqttException {
		super(serverURI, clientId, mdfp);
	}
	
	public void reqPing() {
		try {
			MqttDeliveryToken token = new MqttDeliveryToken(getClientId());
			MqttPingReq pingMsg = new MqttPingReq();
			this.aClient.comms.sendNoWait(pingMsg, token);
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}
}
