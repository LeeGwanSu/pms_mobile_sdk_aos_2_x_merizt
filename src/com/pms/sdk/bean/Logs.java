package com.pms.sdk.bean;

import android.database.Cursor;

/**
 * @since 2014.02.03
 * @author Yang
 * @description API Logs bean
 */
public class Logs {
	
	public static final String TABLE_NAME = "TBL_LOGS";
	public static final String _ID = "_ID";
	public static final String _DATE = "_DATE";
	public static final String _TIME = "_TIME";
	public static final String LOG_TYPE_FLAG = "LOG_TYPE_FLAG";
	public static final String API = "API";
	public static final String PARAM = "PARAM";
	public static final String RESULT = "RESULT";
	public static final String PRIVATELOG = "PRIVATELOG";
	
	// LOG TYPE
	public static final String START = "0";
	public static final String SUCCSESS = "1";
	public static final String FAIL = "2";
	public static final String STOP = "3";
	
	// Private
	public static final String TYPE_P = "P";
	// API
	public static final String TYPE_A = "A";
	
	public static final String CREAT_LOGS = 
			"CREATE TABLE " + TABLE_NAME + "(" 
			+ _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ _DATE + " TEXT, "
			+ _TIME + " TEXT, "
			+ LOG_TYPE_FLAG + " TEXT, "
			+ API + " TEXT, "
			+ PARAM + " TEXT, "
			+ RESULT + " TEXT, "
			+ PRIVATELOG + " TEXT "
			+ ");"; 
	
	public String id = "-1";
	public String date = "";
	public String time = "";
	public String logFlag = "";
	public String api = "";
	public String param = "";
	public String result = "";
	public String privateLog = "";
	
	public Logs() {}
	
	public Logs(Cursor c) {
		
		id = c.getString(c.getColumnIndexOrThrow(_ID));
		logFlag = c.getString(c.getColumnIndexOrThrow(LOG_TYPE_FLAG));
		if (TYPE_A.equals(logFlag)) {
			date = c.getString(c.getColumnIndexOrThrow(_DATE));
			time = c.getString(c.getColumnIndexOrThrow(_TIME));
			api = c.getString(c.getColumnIndexOrThrow(API));
			param = c.getString(c.getColumnIndexOrThrow(PARAM));
			result = c.getString(c.getColumnIndexOrThrow(RESULT));
		} else if (TYPE_P.equals(logFlag)) {
			date = c.getString(c.getColumnIndexOrThrow(_DATE));
			time = c.getString(c.getColumnIndexOrThrow(_TIME));
			privateLog = c.getString(c.getColumnIndexOrThrow(PRIVATELOG));
		}
	}
}
