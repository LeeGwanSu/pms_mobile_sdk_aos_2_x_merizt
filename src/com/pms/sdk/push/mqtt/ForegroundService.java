package com.pms.sdk.push.mqtt;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.pms.sdk.common.util.CLog;

public class ForegroundService extends Service 
{
	static final String ACTION_FOREGROUND = "com.example.android.apis.FOREGROUND";
    static final String ACTION_BACKGROUND = "com.example.android.apis.BACKGROUND";
    
    @SuppressWarnings("rawtypes")
	private static final Class[] mStartForegroundSignature = new Class[] {int.class, Notification.class};
    @SuppressWarnings("rawtypes")
	private static final Class[] mStopForegroundSignature = new Class[] {boolean.class};
    
    private Method mStartForeground;
    private Method mStopForeground;
    private Object[] mStartForegroundArgs = new Object[2];
    private Object[] mStopForegroundArgs = new Object[1];
    
    @Override
    public void onCreate() {
        try {
            mStartForeground = getClass().getMethod("startForeground", mStartForegroundSignature);
            mStopForeground = getClass().getMethod("stopForeground", mStopForegroundSignature);
        } catch (NoSuchMethodException e) {
            mStartForeground = mStopForeground = null;
        }
    }
 
    @Override
    public void onStart(Intent intent, int startId) {
    }
 
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	handleCommand(intent);
        return START_STICKY;
    }
    
    @SuppressWarnings("deprecation")
	private void handleCommand(Intent intent) {
        if (ACTION_FOREGROUND.equals(intent.getAction())) {
            Notification notification = new Notification(0, "Service Start!!", System.currentTimeMillis());
            startForegroundCompat(1, notification);
            
        } else if (ACTION_BACKGROUND.equals(intent.getAction())) {
            stopForegroundCompat(1);
        }
    }
 
    void startForegroundCompat(int id, Notification notification) {
    	CLog.e("startForegroundCompat()");
        if (mStartForeground != null) {
            mStartForegroundArgs[0] = Integer.valueOf(id);
            mStartForegroundArgs[1] = notification;
            try {
                mStartForeground.invoke(this, mStartForegroundArgs);
            } catch (InvocationTargetException e) {
                CLog.w("Unable to invoke startForeground " + e);
            } catch (IllegalAccessException e) {
                CLog.w("Unable to invoke startForeground " + e);
            }
            return;
        }
        
        startForeground(id, notification);
    }
    
    void stopForegroundCompat(int id) {
    	CLog.e("stopForegroundCompat()");
        if (mStopForeground != null) {
            mStopForegroundArgs[0] = Boolean.TRUE;
            try {
                mStopForeground.invoke(this, mStopForegroundArgs);
            } catch (InvocationTargetException e) {
                CLog.w("Unable to invoke stopForeground " + e);
            } catch (IllegalAccessException e) {
                CLog.w("Unable to invoke stopForeground " + e);
            }
            return;
        }
        
        stopForeground(false);
    }
    
    @Override
    public void onDestroy() {
//    	stopForegroundCompat(1);
    }
 
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
